from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate
from rest_framework import serializers
from .models import Profile, Feedback

User = get_user_model()


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'user', 'name', 'last_name', 'phone',
                  'type_user', 'rating', 'is_approved')
        model = Profile


# User Serializer
class UserSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ('id', 'username', 'email')

    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'password')
# Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ('id', 'username', 'email', 'password')
    extra_kwargs = {'password': {'write_only': True}}

  def create(self, validated_data):
    user = User.objects.create_user(validated_data['username'], validated_data['email'], validated_data['password'])

    return user

# Login Serializer
class LoginSerializer(serializers.Serializer):
  username = serializers.CharField()
  password = serializers.CharField()

  def validate(self, data):
    user = authenticate(**data)
    if user and user.is_active:
      return user
    raise serializers.ValidationError("Incorrect Credentials")


class FeedbackCreateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = ('feedback', 'author', 'profile', 'created_date')

class FeedbackListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = ('feedback', 'author', 'profile', 'created_date')

class FeedbackDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = ('id', 'feedback', 'author', 'profile', 'created_date')
