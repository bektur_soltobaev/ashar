from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=25)
    phone = models.CharField(max_length=16)
    is_approved = models.BooleanField(default=True, verbose_name='Проверенный пользователь')
    mark = models.FloatField(default=0)

    def __str__(self):
        return self.user.username

class Feedback(models.Model):
    feedback = models.TextField(max_length=500)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='feedback')
    created_date = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('-created_date',)

    def __str__(self):
        return self.feedback
