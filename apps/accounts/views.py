from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView, UpdateAPIView, DestroyAPIView, RetrieveUpdateAPIView
from rest_framework import views, viewsets, status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response

from rest_framework import viewsets, permissions

from .models import Profile, Feedback
from .permissions import IsAuthorOrReadOnly
from .serializers import ProfileSerializer, FeedbackListSerializer, FeedbackDetailSerializer, FeedbackCreateUpdateSerializer

from drf_yasg.views import get_schema_view
from drf_yasg import openapi


User = get_user_model()


schema_view = get_schema_view(
   openapi.Info(
      title="Ashar API",
      default_version='v1',
      description="Service for finding executors for certain tasks",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

class ProfileViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    lookup_field = 'id'



class FeedbackListAPIView(ListAPIView):
    serializer_class = FeedbackListSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['feedback', 'author', 'created_date']
    pagination_class = LimitOffsetPagination #PageNumberPagination


    def get_queryset(self, *args, **kwargs):
        # queryset_list = super(FeedbackListAPIView, self).get_queryset(*args, **kwargs)
        queryset_list = Feedback.objects.all()
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                Q(feedback_icontains=query)|
                Q(author_icontains=query)|
                Q(created_date_icontains=query)
                ).distinct()
        return queryset_list

class FeedbackDetailAPIView(RetrieveAPIView):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackDetailSerializer


class FeedbackCreateAPIView(CreateAPIView):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackCreateUpdateSerializer

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
    

class FeedbackUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackCreateUpdateSerializer

    def perform_update(self, serializer):
        serializer.save(author=self.request.user)

class FeedbackDeleteAPIView(DestroyAPIView):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackDetailSerializer