from django.urls import include, path
from .api import RegisterAPI, LoginAPI, UserAPI

from rest_framework.routers import DefaultRouter

from knox import views as knox_views
from .views import (
    ProfileViewSet, 
    FeedbackListAPIView,
    FeedbackDetailAPIView,
    FeedbackUpdateAPIView,
    FeedbackDeleteAPIView,
    FeedbackCreateAPIView,
    )


router = DefaultRouter()
router.register(r'profile', ProfileViewSet)


urlpatterns = [

      path('auth', include('knox.urls')),
      path('auth/register', RegisterAPI.as_view()),
      path('auth/login', LoginAPI.as_view()),
      path('auth/user', UserAPI.as_view()),
      path('auth/logout', knox_views.LogoutView.as_view(), name='knox_logout'),

      path('feedbacks/', FeedbackListAPIView.as_view(), name='feedback-list'),
      path('feedback/<int:pk>/', FeedbackDetailAPIView.as_view(), name='feedback-detail'),
      path('feedback/create/', FeedbackCreateAPIView.as_view(), name='feedback-create'),
      path('feedback/<int:pk>/edit/', FeedbackUpdateAPIView.as_view(), name='feedback-update'),
      path('feedback/<int:pk>/delete/', FeedbackDeleteAPIView.as_view(), name='feedback-delete'),
    ]

urlpatterns += router.urls