import datetime

import pytz
from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from .models import Task
from apps.category.models import Category

User = get_user_model()

date_time = datetime.datetime(2012, 12, 12, 20, 20, tzinfo=pytz.UTC)


class TasksTest(APITestCase):

    def setUp(self):
        user = User.objects.create(username='asd', email='asd@asd.com', password='asdfg')
        category = Category.objects.create(name=1)
        task1 = Task.objects.create(name='test', summary='test', estimated_pay=15,
                                    date_time=date_time,
                                    host=user)
        task2 = Task.objects.create(name='test', summary='test', estimated_pay=15,
                                    date_time=date_time, host=user)
        self.user = self.setup_user()


    @staticmethod
    def setup_user():
        User = get_user_model()
        return User.objects.create_user(
            username='test',
            email='testuser@test.com',
            password='test'
        )

    def test_list_of_tasks(self):
        url = reverse('tasks_url')
        self.client.login(username='test', password='test')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_task(self):
        url = reverse('tasks_url')
        self.client.login(username='test', password='test')
        data = {
                "name": "tusk1",
                "summary": "summary1",
                "estimated_pay": 15,
                "phone_number": "779202020",
                "date_time": "20.12.12 20:12"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_task_retrieve(self):
        url = reverse('task_detail_url', args=[1])
        self.client.login(username='test', password='test')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_accept_task(self):
        url = reverse('task_accept_url')
        data = {'task_id': 1}
        self.client.login(username='test', password='test')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
