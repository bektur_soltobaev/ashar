from django.contrib.auth import get_user_model
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, views, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .serializers import TaskSerializer, TaskAcceptSerializer, TaskApproveSerializer
from .models import Task

User = get_user_model()


class TasksModelViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    lookup_field = 'id'


class TaskAcceptView(views.APIView):

    permission_classes = (IsAuthenticated, )

    @swagger_auto_schema(request_body=TaskAcceptSerializer, operation_description='Adds potential executors',
                         responses={200: 'Add / remove executor'})
    def post(self, request):
        serializer = TaskAcceptSerializer(data=request.data)
        if serializer.is_valid():
            user = request.user
            task_id = serializer.validated_data.get('task_id')
            task = Task.objects.get(id=task_id)
            task.potential_executor.add(user)
            return Response({'You applied for': [x.id for x in user.potential_job.all()]}, status=status.HTTP_200_OK)
        return Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class TaskApproveView(views.APIView):
    @swagger_auto_schema(request_body=TaskApproveSerializer, operation_description='Approves executor for task',
                         responses={200: 'Set for task', 405: 'Not host of the task'})
    def post(self, request):
        serializer = TaskApproveSerializer(data=request.data)
        if serializer.is_valid():
            user = request.user
            task_id = serializer.validated_data.get('task_id')
            task = Task.objects.get(id=task_id)
            if task.host == user:
                task.potential_executor.clear()
                executor = User.objects.get(id=serializer.validated_data('executor_id'))
                task.executor.set(executor)
                return Response(f'User {task.executor.username} is set for task {task.name}',
                                status=status.HTTP_200_OK)
            return Response({'You have to be the host of the task to accept executors'},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)
        return Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
