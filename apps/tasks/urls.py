from django.urls import path
from .views import TasksModelViewSet, TaskAcceptView, TaskApproveView

urlpatterns = [
    path('tasks/', TasksModelViewSet.as_view({'get': 'list', 'post': 'create'}), name='tasks_url'),
    path('tasks/<int:id>/', TasksModelViewSet.as_view({'get': 'retrieve', 'put': 'update',
                                                      'delete': 'destroy'}), name='task_detail_url'),
    path('tasks/potential/', TaskAcceptView.as_view(), name='task_accept_url'),
    path('tasks/job/approved/', TaskApproveView.as_view(), name='task_approve_url')
]
