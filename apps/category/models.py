from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=127)

    def __str__(self):
        return str(self.name)

    class Meta:
        ordering = ('name', )
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
